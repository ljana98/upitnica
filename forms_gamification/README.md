#Upitnica#

Upitnica je aplikacija za stvaranje upitnika s elementima igre. Elementi igre korišteni su kako bi se podigla koncentracija i motivacija ispitanika prilikom ispunjavanja upitnika.
Osim ispunjavanja, u aplikaciji je moguća izrada upitnika te pregledavanje odgovora.


Ova aplikacija razvijena je u sklopu završnog rada:

Ana Ljubek, Programsko rješenje zasnovano na tehnologiji Flutter za ispitivanje korisničkog iskustva s elementima igre.

Sveučilište u Zagrebu, Fakultet elektrotehnike i računarstva,ak. god. 2020./2021

Mentorica: prof. dr. sc. Željka Car

Asistentice: dr. sc. Ivana Rašan, Iva Topolovac, mag. ing. 


Slike korištene u aplikaciji preuzete su s Freepika, a dizajnirali su ih korisnici pikisuperstar i macrovector.
