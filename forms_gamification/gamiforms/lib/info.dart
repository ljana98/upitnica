//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gamiforms/home.dart';

class Info extends StatefulWidget {
  Info();

  @override
  _InfoState createState() => _InfoState();
}

class _InfoState extends State<Info> {
  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Informacije',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 10, right: 10, top: 50),
        width: width,
        child: Align(
          child: Column(
            children: <Widget>[
              Text(
                'Informacije o aplikaciji\n',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                'Upitnica je aplikacija za stvaranje upitnika s elementima igre. Elementi igre korišteni su kako bi se podigla koncentracija i motivacija ispitanika prilikom ispunjavanja upitnika.\nOsim ispunjavanja, u aplikaciji je moguća izrada upitnika te pregledavanje odgovora.',
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25,
              ),
              Text(
                'Ova aplikacija razvijena je u sklopu završnog rada:\n\nAna Ljubek, Programsko rješenje zasnovano na tehnologiji Flutter za ispitivanje korisničkog iskustva s elementima igre.\n\nSveučilište u Zagrebu, Fakultet elektrotehnike i računarstva,\nak. god. 2020./2021\n\nMentorica: prof. dr. sc. Željka Car\nAsistentice: dr. sc. Ivana Rašan,\nIva Topolovac, mag. ing.',
                style: TextStyle(fontSize: 16),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 25,
              ),
              Text(
                'Slike korištene u aplikaciji preuzete su s Freepika, a dizajnirali su ih korisnici pikisuperstar i macrovector.',
                style: TextStyle(fontSize: 16),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'U redu',
          child: Icon(Icons.check),
          onPressed: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => (Home())),
            );
          }),
    );
  }
}
