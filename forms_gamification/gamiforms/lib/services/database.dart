import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {
  final String uid;

  DatabaseService({this.uid});

  Future<void> addData(userData) async {
    Firestore.instance.collection("users").add(userData).catchError((e) {
      print(e);
    });
  }

  getData() async {
    return await Firestore.instance.collection("users").snapshots();
  }

  Future<void> addFormData(Map formData, String formId) async {
    await Firestore.instance
        .collection("forms")
        .document(formId)
        .setData(formData)
        .catchError((e) {
      print(e);
    });
  }

  Future<void> addQuestionData(questionData, String formId) async {
    await Firestore.instance
        .collection("forms")
        .document(formId)
        .collection("questions")
        .add(questionData)
        .catchError((e) {
      print(e);
    });
  }

  Future<void> addAnswerData(answerData, String formId) async {
    await Firestore.instance
        .collection("forms")
        .document(formId)
        .collection("answers")
        .add(answerData)
        .catchError((e) {
      print(e);
    });
  }

  getFormData() async {
    return await Firestore.instance.collection("forms").snapshots();
  }

  getOneFormData(String formId) async{
    return await Firestore.instance.collection("forms").document(formId).get();
  }

  getQuestionData(String formId) async{
    return await Firestore.instance
        .collection("forms")
        .document(formId)        
        .collection("questions")
        .getDocuments();
  }

  getAnswerData(String formId) async{
    return await Firestore.instance
        .collection("forms")
        .document(formId)        
        .collection("answers")
        .getDocuments();
  }
}