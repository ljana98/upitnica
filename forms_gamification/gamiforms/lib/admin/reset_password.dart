import 'package:flutter/material.dart';
import 'package:gamiforms/services/auth.dart';

class ResetPassword extends StatefulWidget {
  final Function toogleView;

  ResetPassword({this.toogleView});

  @override
  State<StatefulWidget> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  String email;
  AuthService authService = new AuthService();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Zaboravljena lozinka',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: _buildInputForm(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Zatraži novu lozinku',
          child: Icon(Icons.send),
          onPressed: () {
            authService.resetPass(email);
            Navigator.of(context).pop();
          }),
    );
  }

  Widget _buildInputForm() {
    bool agree = false;
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Text(
            'Unesite e-mail kako biste postavili novu lozinku.',
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 18),
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'E-mail',
              labelStyle: TextStyle(fontSize: 18),
            ),
            autocorrect: false,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) => val.isEmpty ? 'E-mail mora biti unesen' : null,
            onChanged: (val) {
              email = val;
            },
          ),
        ],
      ),
    );
  }
}
