import 'package:flutter/material.dart';
import 'package:gamiforms/admin/log_home.dart';
import 'package:gamiforms/admin/signin.dart';
import 'package:gamiforms/helper/constants.dart';
import 'package:gamiforms/services/auth.dart';

class SignUp extends StatefulWidget {
  final Function toogleView;

  SignUp({this.toogleView});

  @override
  State<StatefulWidget> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  String name, email, password;

  AuthService authService = new AuthService();

  bool _isLoading = false;

  signUp() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });

      await authService
          .signUpWithEmailAndPassword(email, password)
          .then((value) {
        if (value != null) {
          setState(() {
            _isLoading = false;
          });
          Constants.saveUserLoggedInDetails(isLoggedIn: true);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => LogHome()));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Registracija',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: _buildInputForm(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Registriraj se',
          child: Icon(Icons.login),
          onPressed: () {
            signUp();
          }),
    );
  }

  Widget _buildInputForm() {
    bool agree = false;
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Ime',
              labelStyle: TextStyle(fontSize: 18),
            ),
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) => val.isEmpty ? 'Ime mora biti uneseno' : null,
            onChanged: (val) {
              name = val;
            },
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'E-mail',
              labelStyle: TextStyle(fontSize: 18),
            ),
            autocorrect: false,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) => val.isEmpty ? 'E-mail mora biti unesen' : null,
            onChanged: (val) {
              email = val;
            },
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Lozinka',
              labelStyle: TextStyle(fontSize: 18),
            ),
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) =>
                val.isEmpty ? 'Lozinka mora biti unesena' : null,
            onChanged: (val) {
              password = val;
            },
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Napomena: lozinka mora sadržavati minimalno 6 znakova.',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 14,
              color: Colors.black.withOpacity(0.5),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('\nVeć imate korisnički račun? ',
                  style: TextStyle(color: Colors.black87, fontSize: 17)),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignIn()));
                },
                child: Container(
                  child: Text('\nPrijavite se',
                      style: TextStyle(
                          color: Colors.black87,
                          decoration: TextDecoration.underline,
                          fontSize: 17)),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
