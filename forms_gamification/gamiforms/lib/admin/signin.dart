import 'package:flutter/material.dart';
import 'package:gamiforms/admin/reset_password.dart';
import 'package:gamiforms/admin/signup.dart';
import 'package:gamiforms/helper/constants.dart';
import 'package:gamiforms/main.dart';
import 'package:gamiforms/services/auth.dart';

class SignIn extends StatefulWidget {
  final Function toogleView;

  SignIn({this.toogleView});

  @override
  State<StatefulWidget> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  String email, password;
  AuthService authService = new AuthService();

  bool _isLoading = false;

  signIn() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });

      await authService.signInEmailAndPass(email, password).then((val) {
        if (val != null) {
          setState(() {
            _isLoading = false;
          });
          Constants.saveUserLoggedInDetails(isLoggedIn: true);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => MyApp()));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Prijava',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              child: Column(
                children: <Widget>[
                  Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: _buildInputForm(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Prijavi se',
          child: Icon(Icons.login),
          onPressed: () {
            signIn();
          }),
    );
  }

  Widget _buildInputForm() {
    bool agree = false;
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              labelText: 'E-mail',
              labelStyle: TextStyle(fontSize: 18),
            ),
            autocorrect: false,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) => val.isEmpty ? 'E-mail mora biti unesen' : null,
            onChanged: (val) {
              email = val;
            },
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
              labelText: 'Lozinka',
              labelStyle: TextStyle(fontSize: 18),
            ),
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) =>
                val.isEmpty ? 'Lozinka mora biti unesena' : null,
            onChanged: (val) {
              password = val;
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('\nJoš nemate korisnički račun? ',
                  style: TextStyle(color: Colors.black87, fontSize: 17)),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignUp()));
                },
                child: Container(
                  child: Text(
                    '\nRegistrirajte se',
                    style: TextStyle(
                        color: Colors.black87,
                        decoration: TextDecoration.underline,
                        fontSize: 17),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => (ResetPassword())),
                  );
                },
                child: Text(
                  'Zaboravili ste lozinku?',
                  style: TextStyle(
                      color: Colors.teal,
                      fontSize: 14,
                      decoration: TextDecoration.underline),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
