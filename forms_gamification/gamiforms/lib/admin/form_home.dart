import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gamiforms/admin/create_form/agreement_default.dart';
import 'package:gamiforms/admin/load_answers.dart';
import 'package:gamiforms/admin/log_home.dart';
import 'package:gamiforms/admin/preview_form.dart';
import 'package:gamiforms/services/database.dart';

class AdminFormHome extends StatefulWidget {
  final String formId;
  AdminFormHome(this.formId);

  @override
  _AdminFormHomeState createState() => _AdminFormHomeState(formId);
}

int total = 0;
int _notAttempted = 0;

Stream infoStream;

class _AdminFormHomeState extends State<AdminFormHome> {
  var current_avatar = 'male';

  String formId;

  _AdminFormHomeState(this.formId);

  DatabaseService databaseService = DatabaseService();
  Stream formStream;
  QuerySnapshot questionSnapshot;

  DocumentSnapshot snapshot;

  bool isLoading = true;

  @override
  void initState() {
    print("${widget.formId}");

    databaseService.getFormData().then((value) {
      formStream = value;
      setState(() {});
    });

    databaseService.getQuestionData(widget.formId).then((value) {
      questionSnapshot = value;
      total = questionSnapshot.documents.length;
      isLoading = false;

      print("$total this is total ${widget.formId}");

      setState(() {});
    });

    super.initState();
  }

  bool printed = false;
  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');
    //print("tu sam $context");
    return FutureBuilder<DocumentSnapshot>(
      future: questionairres.document(formId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data;
          var screenSize = MediaQuery.of(context).size;
          var width = screenSize.width;
          return Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              title: Center(
                child: Text(
                  'Upitnici',
                  textAlign: TextAlign.center,
                ),
              ),
              backgroundColor: Colors.teal,
            ),
            body: Container(
              child: ListView(
                children: [
                  FillFormTile(
                    title: data['formTitle'],
                    imageUrl: data['formImageUrl'],
                    description: data['formDesc'],
                    id: data['formId'],
                    duration: data['formDuration'],
                    agree: data['agree'],
                  ),
                ],
              ),
            ),
            floatingActionButton: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FloatingActionButton(
                  tooltip: 'Pregled odgovora',
                  child: Icon(Icons.ballot),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LoadAnswers(widget.formId)));
                  },
                ),
                SizedBox(
                  width: 10,
                ),
                FloatingActionButton(
                  tooltip: 'Završi pregled upitnika',
                  child: Icon(Icons.check),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => (LogHome()),
                      ),
                    );
                  },
                ),
              ],
            ),
          );
        }
      },
    );
  }
}

class FillFormTile extends StatelessWidget {
  final String title, imageUrl, id, description;
  final int duration;
  final bool agree;

  FillFormTile({
    this.title,
    this.imageUrl,
    this.description,
    this.id,
    this.duration,
    this.agree,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Stack(
                children: [Image.network(imageUrl, fit: BoxFit.cover)],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              description,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          ),
          Text(
            'ID upitnika: $id ',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 30),
          Text(
            'Trajanje upitnika: $duration min.',
            style: TextStyle(
              fontSize: 16,
            ), 
          ),
          SizedBox(height: 30),
          agree == true
              ? Column(
                  children: [
                    Text(
                      'Korištenje privole: DA',
                      style: TextStyle(
                        fontSize: 16,
                      ), 
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AgreementDefault()));
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          '\nPrivola o sudjelovanju u upitniku',
                          style: TextStyle(
                              color: Colors.black87,
                              decoration: TextDecoration.underline,
                              fontSize: 14),
                        ),
                      ),
                    ),
                  ],
                )
              : Text(
                  'Korištenje privole: NE',
                  style: TextStyle(
                    fontSize: 16,
                  ), 
                ),
          SizedBox(height: 20),
          RaisedButton(
              color: Colors.teal,
              textColor: Colors.white,
              child: Text('Pregledaj upitnik'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => (PreviewForm(id))),
                );
              }),
          SizedBox(height: 100),
        ],
      ),
    );
  }
}
