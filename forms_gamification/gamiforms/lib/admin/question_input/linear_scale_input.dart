import 'package:flutter/material.dart';
import 'package:gamiforms/admin/create_form/pick_question_type.dart';
import 'package:gamiforms/services/database.dart';
import 'package:random_string/random_string.dart';

import '../log_home.dart';

class LinearScaleInput extends StatefulWidget {
  final String formId;
  LinearScaleInput(this.formId);

  @override
  _LinearScaleInputState createState() => _LinearScaleInputState(formId);
}

class _LinearScaleInputState extends State<LinearScaleInput> {
  
  final String formId;
  _LinearScaleInputState(this.formId);

  DatabaseService databaseService = DatabaseService();

  final _formKey = GlobalKey<FormState>();

  bool isLoading = false;

  String questionId = "",
      question = "",
      questionType = "";

  uploadFormData() {
    if (_formKey.currentState.validate()) {
      setState(() {
        isLoading = true;
      });

      questionId = randomAlphaNumeric(16);
      
      questionType ="LS";

      Map<String, String> questionMap = {
        "questionId": questionId,
        "question": question,
        "questionType": questionType,
      };

      print("${widget.formId}");
      databaseService.addQuestionData(questionMap, widget.formId).then((value) {
        questionId = "";
        question = "";
        questionType = "";
        setState(() {
          isLoading = false;
        });
      }).catchError((e) {
        print(e);
      });
    } else {
      print("error is happening ");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal[50],
      appBar: AppBar(
        title: Center(
          child: Text(
            'Novo pitanje',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: isLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: ListView(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        '\nUnesite tekst pitanja:',
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    TextFormField(
                      validator: (val) =>
                          val.isEmpty ? "Tekst pitanja mora biti unesen" : null,
                      decoration: InputDecoration(hintText: "Tekst pitanja"),
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      onChanged: (val) {
                        question = val;
                      },
                    ),
                  ],
                ),
              ),
            ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              tooltip: 'Spremi upitnik',
              child: Icon(Icons.save),
              onPressed: () {
                uploadFormData();
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => (LogHome())));
              }),
          SizedBox(
            width: 10,
          ),
          FloatingActionButton(
              tooltip: 'Dodaj pitanje',
              child: Icon(Icons.add_circle_outline),
              onPressed: () {
                uploadFormData();
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => (PickQuestionType(formId))));
              }),
        ],
      ),
    );
  }
}
