import 'package:flutter/material.dart';
import 'package:gamiforms/admin/form_home.dart';
import 'package:gamiforms/home.dart';
import 'package:gamiforms/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:gamiforms/helper/constants.dart';

import 'create_form/new_form.dart';

class LogHome extends StatefulWidget {
  @override
  _LogHomeState createState() => _LogHomeState();
}

class _LogHomeState extends State<LogHome> {
  Stream formStream;
  DatabaseService databaseService = DatabaseService();

  Widget formList(String currentUser) {
    return Container(
      child: ListView(
        children: [
          StreamBuilder(
            stream: formStream,
            builder: (context, snapshot) {
              return snapshot.data == null
                  ? Container(child: Text('Pritisnite gumb + kako biste stvorili upitnik.'),)
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        return FormTile(
                                currentUser: currentUser,
                                userId: snapshot
                                    .data.documents[index].data['userId'],
                                imageUrl: snapshot
                                    .data.documents[index].data['formImageUrl'],
                                title: snapshot
                                    .data.documents[index].data['formTitle'],
                                description: snapshot
                                    .data.documents[index].data['formDesc'],
                                id: snapshot
                                    .data.documents[index].data["formId"],
                              )
                            ;
                      });
            },
          )
        ],
      ),
    );
  }

  getLoggedInState() async {
    await Constants.getUserLoggedInSharedPreference().then((value) {
      print('VALUE $value');
    });
  }

  @override
  void initState() {
    getLoggedInState();

    databaseService.getFormData().then((value) {
      formStream = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'Upitnici',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.logout),
              onPressed: () {
                signOut();
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (context) => Home()));
              })
        ],
      ),
      body: FutureBuilder(
        future: FirebaseAuth.instance.currentUser(),
        builder: (context, AsyncSnapshot<FirebaseUser> snapshot) {
          if (snapshot.hasData) {
            return formList(snapshot.data.uid);
          } else {
            return Container();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Novi upitnik',
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => NewForm()));
        },
      ),
    );
  }

  FirebaseAuth _auth = FirebaseAuth.instance;

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}

class FormTile extends StatelessWidget {
  final String imageUrl, title, id, description, userId, currentUser;

  FormTile({
    @required this.currentUser,
    @required this.userId,
    @required this.title,
    @required this.imageUrl,
    @required this.description,
    @required this.id,
  });

  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    print("userId $userId");
    print("current user $currentUser");
    return userId == currentUser ?
     GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AdminFormHome(id)),
        );
      },
      child: Container(
        padding: EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 10),
        height: 170,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Stack(
            children: [
              Image.network(
                imageUrl,
                fit: BoxFit.cover,
                width: MediaQuery.of(context).size.width,
              ),
              Container(
                color: Colors.black26,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        title,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        description,
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "ID: " + id,
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ) : Container();
  }
}