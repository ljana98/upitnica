import 'package:flutter/material.dart';
import 'package:gamiforms/admin/question_input/check_uncheck_input.dart';
import 'package:gamiforms/admin/question_input/like_dislike_input.dart';
import 'package:gamiforms/admin/question_input/yes_no_input.dart';

class PickYesNoType extends StatelessWidget {
  var current_avatar = 'male';

  final String formId;
  PickYesNoType(this.formId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Novo pitanje',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Text(
                  'Odaberite tip DA/NE pitalice:',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('DA/NE'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (YesNoInput(formId))),
                    );
                  }),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Sviđa mi se/ne sviđa mi se'), 
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (LikeDislikeInput(formId))),
                    );
                  }),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Kvačica/križić'), 
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (CheckUncheckInput(formId))),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
