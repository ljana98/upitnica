import 'package:flutter/material.dart';
import 'package:gamiforms/admin/create_form/pick_linear_scale_type.dart';
import 'package:gamiforms/admin/create_form/pick_yes_no_type.dart';
import 'package:gamiforms/admin/question_input/select_answer_input.dart';
import 'package:gamiforms/admin/question_input/text_box_input.dart';

class PickQuestionType extends StatelessWidget {
  final String formId;
  PickQuestionType(this.formId);

  var current_avatar = 'male';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Novo pitanje',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Text(
                  'Odaberite tip pitanja:',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Tekstualni odgovor'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (TextBoxInput(formId))),
                    );
                  }),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Višestruki odabir'), //select_answer
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (SelectAnswerInput(formId))),
                    );
                  }),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Linearno mjerilo'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (PickLinearScaleType(formId))),
                    );
                  }),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('DA/NE pitalica'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (PickYesNoType(formId))),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
