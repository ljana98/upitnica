import 'package:flutter/material.dart';
import 'package:gamiforms/admin/question_input/linear_emoji_input.dart';
import 'package:gamiforms/admin/question_input/linear_grade_input.dart';
import 'package:gamiforms/admin/question_input/linear_scale_input.dart';

class PickLinearScaleType extends StatelessWidget {
  var current_avatar = 'male';

  final String formId;
  PickLinearScaleType(this.formId);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Novo pitanje',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Text(
                  'Odaberite tip linearnog mjerila:',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Ocjena'), 
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (LinearGradeInput(formId))),
                    );
                  }),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Odlično/loše'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (LinearScaleInput(formId))),
                    );
                  }),
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: RaisedButton(
                  color: Colors.teal,
                  textColor: Colors.white,
                  child: Text('Emotikoni'), 
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => (LinearEmojiInput(formId))),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
