import 'package:flutter/material.dart';
import 'package:gamiforms/admin/create_form/pick_question_type.dart';
import 'package:gamiforms/services/database.dart';
import 'package:random_string/random_string.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../log_home.dart';
import 'agreement_default.dart';

class NewForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NewFormState();
}

class _NewFormState extends State<NewForm> {
  final _formKey = GlobalKey<FormState>();

  String formImageUrl, formTitle, formDesc, userId;
  int formDuration;
  bool agree = false;

  bool _isLoading = false;
  String formId;

  DatabaseService databaseService = DatabaseService();

  createFormOnline() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      formId = randomAlphaNumeric(6);

      Map<String, dynamic> formMap = {
        "formId": formId,
        "formTitle": formTitle,
        "formImageUrl": formImageUrl,
        "formDesc": formDesc,
        "agree": agree,
        "formDuration": formDuration,
        "userId": userId,
      };

      await databaseService.addFormData(formMap, formId).then((value) {
        setState(() {
          _isLoading = false;
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => PickQuestionType(formId),
            ),
          );
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Stvori upitnik',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: _isLoading
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Form(
              child: Container(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    Center(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(20),
                            child: _buildInputForm(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
              tooltip: 'Spremi',
              child: Icon(Icons.save),
              onPressed: () {
                createFormOnline();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => (LogHome())));
              }),
          SizedBox(
            width: 10,
          ),
          FloatingActionButton(
            tooltip: 'Novo pitanje',
            child: Icon(Icons.add_circle_outline),
            onPressed: () {
            },
          ),
        ],
      ),
    );
  }

  Widget _buildInputForm() {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    
    bool isNumeric(String s) {
      if (s == null) {
        return false;
      }
      return int.tryParse(s) != null;
    }

    return FutureBuilder(
      future: FirebaseAuth.instance.currentUser(),
      builder: (context, AsyncSnapshot<FirebaseUser> snapshot) {
        if (snapshot.hasData) {
          userId = snapshot.data.uid;
          return Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Unesite podatke o upitniku:',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Naslov upitnika',
                    labelStyle: TextStyle(fontSize: 18),
                  ),
                  style: TextStyle(fontSize: 20, color: Colors.black),
                  validator: (val) =>
                      val.isEmpty ? 'Naslov upitnika mora biti unesen' : null,
                  onChanged: (val) {
                    formTitle = val;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Opis upitnika',
                    labelStyle: TextStyle(fontSize: 18),
                  ),
                  style: TextStyle(fontSize: 20, color: Colors.black),
                  validator: (val) =>
                      val.isEmpty ? 'Opis upitnika mora biti unesen' : null,
                  onChanged: (val) {
                    formDesc = val;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Poveznica na sliku upitnika',
                    labelStyle: TextStyle(fontSize: 18),
                  ),
                  style: TextStyle(fontSize: 20, color: Colors.black),
                  validator: (val) =>
                      val.isEmpty ? 'Poveznica na sliku upitnika mora biti unesena' : null,
                  onChanged: (val) {
                    formImageUrl = val;
                  },
                ),
                Container(
                  margin: const EdgeInsets.only(left: 1.0, top: 15.0),
                  child: Align(
                    alignment: Alignment(10.0, 100.0),
                    child: Row(
                      children: [
                        Material(
                          color: Colors.teal[50],
                          child: Checkbox(
                            value: agree,
                            onChanged: (value) {
                              setState(() {
                                agree = value;
                              });
                            },
                          ),
                        ),
                        Container(
                          width: width - 100,
                          margin: EdgeInsets.only(left: 10.0),
                          child: Column(
                            children: [
                              Text(
                                'Želim koristiti predložak privole o sudjelovanju u upitniku.',
                                overflow: TextOverflow.visible,
                                style: TextStyle(fontSize: 18),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AgreementDefault()));
                                },
                                child: Container(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    '\nPredložak',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        decoration: TextDecoration.underline,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Predviđeno trajanje upitnika (min.)',
                    labelStyle: TextStyle(fontSize: 18),
                  ),
                  keyboardType: TextInputType.number,
                  style: TextStyle(fontSize: 20, color: Colors.black),
                  validator: (val) {
                    if (val.isEmpty)
                      return 'Trajanje upitnika mora biti uneseno';
                    if (!isNumeric(val))
                      return 'Trajanje upitnika mora biti cijeli broj';
                    return null;
                  },
                  onChanged: (val) {
                    formDuration = int.parse(val);
                  },
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
