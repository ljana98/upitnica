import 'package:flutter/material.dart';

class AgreementDefault extends StatefulWidget {
  AgreementDefault();

  @override
  _AgreementDefaultState createState() => _AgreementDefaultState();
}

class _AgreementDefaultState extends State<AgreementDefault> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Predložak privole',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      //body: Text('This is my default text!'),
      //stavljamo djecu za vise widgeta unutar jednog -> stablo
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Predložak privole o sudjelovanju u upitniku\n',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Container(
            width: 300,
            child: Text(
              'Ovaj upitnik je sastavljen za potrebe istraživanja. Cilj je prikupiti podatke i napraviti analizu kako bi se moglo djelovati u području za koje je istraživanje vezano. Vaši odgovori će biti u potpunosti povjerljivi i analizirani isključivo na grupnoj razini. U upitniku se ne zahtijeva unos Vaših osobnih podataka te je anonimnost zajamčena. U svakom trenutku možete odustati od sudjelovanja u ovom istraživanju. Svi odgovori će biti sigurno pohranjeni, a pristup podacima će biti omogućen jedino istraživačima.\n',
              style: TextStyle(fontSize: 16),
            ),
          ),
          Container(
            color: Colors.teal[50],
            child: Align(
              alignment: Alignment(10.0, 100.0),
              child: Row(
                children: [
                  Material(
                    color: Colors.teal[50],
                    child: Checkbox(
                      value: true,
                      onChanged: (value) {
                        null;
                      },
                    ),
                  ),
                  Text(
                    'Pristajem sudjelovati u ovom upitniku.',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 18),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
