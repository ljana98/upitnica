import 'package:flutter/material.dart';
import 'package:gamiforms/admin/load_answers.dart';
import 'package:gamiforms/questions/check_uncheck.dart';
import 'package:gamiforms/questions/linear_grade.dart';
import 'package:gamiforms/questions/text_box.dart';
import 'package:gamiforms/questions/like_dislike.dart';
import 'package:gamiforms/questions/linear_emoji.dart';
import 'package:gamiforms/questions/linear_scale.dart';
import 'package:gamiforms/questions/select_answer.dart';
import 'package:gamiforms/questions/yes_no.dart';
import 'package:gamiforms/admin/log_home.dart';
import 'package:gamiforms/models/question_model.dart';
import 'package:gamiforms/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PreviewForm extends StatefulWidget {
  final String formId;
  PreviewForm(this.formId);

  @override
  _PreviewFormState createState() => _PreviewFormState();
}

int total = 0;

Stream infoStream;

class _PreviewFormState extends State<PreviewForm> {
  DatabaseService databaseService = DatabaseService();
  QuerySnapshot questionSnapshot;

  bool isLoading = true;

  @override
  void initState() {
    print("${widget.formId}");
    databaseService.getQuestionData(widget.formId).then((value) {
      questionSnapshot = value;
      total = questionSnapshot.documents.length;
      isLoading = false;

      print("$total this is total ${widget.formId}");

      setState(() {});
    });
    super.initState();
  }

  QuestionModel getQuestionModelFromDatasnapshot(
      DocumentSnapshot questionSnapshot) {
    QuestionModel questionModel = QuestionModel();

    questionModel.question = questionSnapshot.data["question"];

    questionModel.option1 = "";
    questionModel.option2 = "";
    questionModel.option3 = "";
    questionModel.option4 = "";

    if (questionSnapshot.data["questionType"] == "CB" ||
        questionSnapshot.data["questionType"] == "SA") {
      questionModel.option1 = questionSnapshot.data["ans1"];
      questionModel.option2 = questionSnapshot.data["ans2"];
      questionModel.option3 = questionSnapshot.data["ans3"];
      questionModel.option4 = questionSnapshot.data["ans4"];
    }

    questionModel.questionType = questionSnapshot.data["questionType"];

    return questionModel;
  }

  @override
  void dispose() {
    infoStream = null;
    super.dispose();
  }

  var currentPageValue = 0;

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            "Upitnik",
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: isLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: <Widget>[
                PageView.builder(
                  scrollDirection: Axis.horizontal,
                  controller: controller,
                  itemCount: questionSnapshot.documents.length,
                  onPageChanged: (int page) {
                    getChangedPageAndMoveBar(page);
                  },
                  itemBuilder: (context, index) {
                    return FillFormTile(
                      formId: widget.formId,
                      questionModel: getQuestionModelFromDatasnapshot(
                          questionSnapshot.documents[index]),
                      index: index,
                    );
                  },
                ),
                Stack(
                  alignment: AlignmentDirectional.topStart,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 100),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          for (int i = 0;
                              i < questionSnapshot.documents.length;
                              i++)
                            if (i == currentPageValue) ...[circleBar(true)] else
                              circleBar(false),
                        ],
                      ),
                    ),
                  ],
                ),
                Stack(
                  children: <Widget>[
                    Positioned(
                      bottom: 20,
                      right: 20,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FloatingActionButton(
                            tooltip: "Pregledaj odgovore",
                            child: Icon(Icons.ballot),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoadAnswers(widget.formId)));
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          FloatingActionButton(
                            tooltip: 'Završi pregled upitnika',
                            child: Icon(Icons.check),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => (LogHome()),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
    );
  }

  void getChangedPageAndMoveBar(int page) {
    currentPageValue = page;
    setState(() {});
  }

  Widget circleBar(bool isActive) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      height: isActive ? 12 : 8,
      width: isActive ? 12 : 8,
      decoration: BoxDecoration(
          color: isActive ? Colors.teal : Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(12))),
    );
  }
}

class FillFormTile extends StatefulWidget {
  final QuestionModel questionModel;
  final String formId;
  final int index;

  FillFormTile(
      {@required this.formId,
      @required this.questionModel,
      @required this.index,
      title});

  @override
  _FillFormTileState createState() => _FillFormTileState();
}

class _FillFormTileState extends State<FillFormTile> {
  String optionSelected = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Container(
            child: widget.questionModel.questionType == 'LG'
                ? LinearGrade(widget.questionModel.question, widget.formId)
                : widget.questionModel.questionType == 'LS'
                    ? LinearScale(widget.questionModel.question, widget.formId)
                    : widget.questionModel.questionType == 'LE'
                        ? LinearEmoji(
                            widget.questionModel.question, widget.formId)
                        : widget.questionModel.questionType == 'CU'
                            ? CheckUncheck(
                                widget.questionModel.question, widget.formId)
                            : widget.questionModel.questionType == 'LD'
                                ? LikeDislike(widget.questionModel.question,
                                    widget.formId)
                                : widget.questionModel.questionType == 'TB'
                                    ? TextBox(widget.questionModel.question,
                                        widget.formId)
                                    : widget.questionModel.questionType == 'YN'
                                        ? YesNo(widget.questionModel.question,
                                            widget.formId)
                                        : widget.questionModel.questionType ==
                                                'SA'
                                            ? SelectAnswer(widget.questionModel,
                                                widget.formId)
                                            /*: widget.questionModel.questionType ==
                                                'CB'
                                            ? Checkboxes(widget.questionModel)*/
                                            : Text(
                                                widget.questionModel.question),
          ),
          SizedBox(height: 4),
        ],
      ),
    );
  }
}

class GetQuestion extends StatelessWidget {
  final String documentId;

  GetQuestion(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');

    return FutureBuilder<QuerySnapshot>(
      future: questionairres
          .document(documentId)
          .collection('questions')
          .getDocuments(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.documents.first.data;
          return data['question'];
        }
      },
    );
  }
}

class GetQuestionType extends StatelessWidget {
  final String documentId;

  GetQuestionType(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');

    return FutureBuilder<QuerySnapshot>(
      future: questionairres
          .document(documentId)
          .collection('questions')
          .getDocuments(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.documents.first.data;
          return data['questionType'];
        }
      },
    );
  }
}

class Duration extends StatelessWidget {
  final String documentId;

  Duration(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');

    return FutureBuilder<DocumentSnapshot>(
      future: questionairres.document(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data;
          return Container(
            width: 300,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                'Trajanje upitnika: ${data['formDuration']} min.',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          );
        }
      },
    );
  }
}
