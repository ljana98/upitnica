import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gamiforms/admin/log_home.dart';
import 'package:gamiforms/models/answer_model.dart';
import 'package:gamiforms/services/database.dart';

class LoadAnswers extends StatefulWidget {
  final String formId;
  LoadAnswers(this.formId);

  @override
  _LoadAnswersState createState() => _LoadAnswersState();
}

Stream infoStream;

class _LoadAnswersState extends State<LoadAnswers> {
  DatabaseService databaseService = DatabaseService();
  QuerySnapshot answerSnapshot;

  bool isLoading = true;

  List<AnswerModel> models;
  bool sortQuestion;
  bool sortAnswer;
  bool sort;
  bool sortColumnIndex;

  @override
  void initState() {
    print("${widget.formId}");
    databaseService.getAnswerData(widget.formId).then((value) {
      answerSnapshot = value;
      int total = answerSnapshot.documents.length;
      isLoading = false;

      print("$total this is total ${widget.formId}");

      sortQuestion = false;
      sortAnswer = false;
      models = getModelsList();

      print("lista modela $models");
      setState(() {});
    });
    super.initState();
  }

  AnswerModel getAnswerModelFromDatasnapshot(DocumentSnapshot answerSnapshot) {
    AnswerModel answerModel = AnswerModel();

    answerModel.question = answerSnapshot.data["question"];
    answerModel.answer = answerSnapshot.data["answer"];

    return answerModel;
  }

  @override
  void dispose() {
    infoStream = null;
    super.dispose();
  }

  List<AnswerModel> getModelsList() {
    AnswerModel model;

    return List<AnswerModel>.generate(
      answerSnapshot.documents.length,
      (index) => model = getAnswerModelFromDatasnapshot(
        answerSnapshot.documents[index],
      ),
    );
  }

  onSortQuestion(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        models.sort((a, b) => a.question.compareTo(b.question));
      } else {
        models.sort((a, b) => b.question.compareTo(a.question));
      }
    }
  }

  onSortAnswer(int columnIndex, bool ascending) {
    if (columnIndex == 1) {
      if (ascending) {
        models
            .sort((a, b) => a.answer.toString().compareTo(b.answer.toString()));
      } else {
        models
            .sort((a, b) => b.answer.toString().compareTo(a.answer.toString()));
      }
    }
  }

  DataRow buildRows(AnswerModel answerModel) {
    String question = answerModel.question;
    String answer = answerModel.answer;
    print("answer model pitanje ${question}");
    print("answer model odgovor ${answer}");
    return DataRow(
      cells: [
        DataCell(
          Text(
            question,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
        DataCell(
          Text(
            answer,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "Odgovori",
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: isLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  DataTable(
                    sortColumnIndex: 0,
                    sortAscending: sortQuestion,
                    columns: <DataColumn>[
                      DataColumn(
                        label: Text(
                          'Pitanje',
                          style: TextStyle(
                              fontSize: 20, fontStyle: FontStyle.italic),
                        ),
                        numeric: false,
                        onSort: (columnIndex, ascending) {
                          setState(() {
                            sortQuestion = !sortQuestion;
                          });
                          onSortQuestion(columnIndex, ascending);
                        },
                      ),
                      DataColumn(
                        label: Text(
                          'Odgovor',
                          style: TextStyle(
                              fontSize: 20, fontStyle: FontStyle.italic),
                        ),
                        numeric: false,
                        onSort: (columnIndex, ascending) {
                          setState(() {
                            sortAnswer = !sortAnswer;
                          });
                          onSortAnswer(columnIndex, ascending);
                        },
                      ),
                    ],
                    rows: List<DataRow>.generate(models.length, (index) {
                      return DataRow(
                        cells: [
                          DataCell(
                            Text(
                              models[index].question.toString(),
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ),
                          DataCell(
                            Text(
                              models[index].answer.toString(),
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      );
                    }),
                  ),
                ],
              ),
            ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Završi pregled upitnika',
        child: Icon(Icons.check),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => (LogHome()),
            ),
          );
        },
      ),
    );
  }
}
