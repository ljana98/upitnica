import 'package:flutter/material.dart';
import 'package:gamiforms/admin/signin.dart';

import 'package:gamiforms/info.dart';
import 'package:gamiforms/user/user_form_home.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  String formId;

  final myController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Center(
            child: Text(
              'Upitnica',
              textAlign: TextAlign.center,
            ),
          ),
          backgroundColor: Colors.teal,
        ),
        body: Center(
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Text(
                    'Unesite ID kako biste započeli s rješavanjem upitnika:',
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 19),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                  child: TextFormField(
                    controller: myController,
                    decoration: InputDecoration(
                      labelText: 'ID upitnika',
                      labelStyle: TextStyle(fontSize: 18),
                    ),
                    style: TextStyle(fontSize: 20, color: Colors.black),
                    validator: (val) => val.isEmpty || val == null
                        ? 'ID upitnika mora biti unesen'
                        : null,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 90, right: 90),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.teal),
                    child: Text('Započni rješavanje upitnika'),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                (UserFormHome(myController.text)),
                          ),
                        );
                      }
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(20),
                  child: Image.asset('assets/images/home.jpg'),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FloatingActionButton(
                tooltip: 'Informacije',
                child: Icon(Icons.info),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => (Info())),
                  );
                }),
            SizedBox(
              width: width - 145,
            ),
            FloatingActionButton(
                tooltip: 'Prijava',
                child: Icon(Icons.account_circle),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => (SignIn())),
                  );
                }),
          ],
        ));
  }
}