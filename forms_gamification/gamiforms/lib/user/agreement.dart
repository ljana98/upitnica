//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gamiforms/home.dart';
import 'package:gamiforms/models/form_model.dart';
import 'package:gamiforms/services/database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:gamiforms/user/avatars.dart';

import 'name.dart';

class Agreement extends StatefulWidget {
  String formId;
  Agreement(this.formId);

  @override
  _AgreementState createState() => _AgreementState(formId);
}

class _AgreementState extends State<Agreement> {
  String formId;
  _AgreementState(this.formId);

  //var document = loadDocuments() as DocumentSnapshot;

  int formDuration;

  static loadDocuments(String formId) async {
    var document = await Firestore.instance.collection("/forms").getDocuments();

    //tu treba dohvatiti upitnik po IDu
    return document.documents
        .firstWhere((element) => element.documentID == formId);
  }

  Function() {
    var document = loadDocuments(this.formId) as DocumentSnapshot;
    formDuration = document.data["formDuration"];
  }

  // By defaut, the checkbox is unchecked and "agree" is "false"
  bool agree = false;

  // This function is triggered when the button is clicked
  void _doSomething() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => (Avatars(formId))),
    );
  }

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');
    //print("tu sam $context");
    return FutureBuilder<DocumentSnapshot>(
        future: questionairres.document(formId).get(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> data = snapshot.data.data;
            var screenSize = MediaQuery.of(context).size;
            var width = screenSize.width;
            return data['agree'] == false ? Avatars(formId) : Scaffold(
                    resizeToAvoidBottomInset: false,
                    appBar: AppBar(
                      title: Center(
                        child: Text(
                          'Upitnik',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      backgroundColor: Colors.teal,
                    ),
                    //body: Text('This is my default text!'),
                    //stavljamo djecu za vise widgeta unutar jednog -> stablo
                    body: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 300,
                          child: Text(
                            'Ovaj upitnik je sastavljen za potrebe istraživanja. Cilj je prikupiti podatke i napraviti analizu kako bi se moglo djelovati u području za koje je istraživanje vezano. Vaši odgovori će biti u potpunosti povjerljivi i analizirani isključivo na grupnoj razini. U upitniku se ne zahtijeva unos Vaših osobnih podataka te je anonimnost zajamčena. U svakom trenutku možete odustati od sudjelovanja u ovom istraživanju. Svi odgovori će biti sigurno pohranjeni, a pristup podacima će biti omogućen jedino istraživačima.\n',
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Container(
                          width: 300,
                          child: Text(
                            'Trajanje upitnika: ${data['formDuration']} min.',
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Container(
                          color: Colors.teal[50],
                          child: Align(
                            alignment: Alignment(10.0, 100.0),
                            child: Row(
                              children: [
                                Material(
                                  color: Colors.teal[50],
                                  child: Checkbox(
                                    value: agree,
                                    onChanged: (value) {
                                      setState(() {
                                        agree = value;
                                      });
                                    },
                                  ),
                                ),
                                Text(
                                  'Pristajem sudjelovati u ovom upitniku.',
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(fontSize: 18),
                                )
                              ],
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: agree ? _doSomething : null,
                          child: Text('Nastavi'),
                        ),
                      ],
                    ),
                  );
          }
        });
  }
}
/*
class Duration extends StatelessWidget {
  final String documentId;

  Duration(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');

    return FutureBuilder<DocumentSnapshot>(
      future: questionairres.document(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data;
          return data == null
              ? Text('wrong')
              : Container(
                  width: 300,
                  child: Text(
                    'Trajanje upitnika: ${data['formDuration']} min.',
                    style: TextStyle(fontSize: 16),
                  ),
                );
        }
      },
    );
  }
}
*/
