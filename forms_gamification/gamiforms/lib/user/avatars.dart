import 'package:flutter/material.dart';

import 'name.dart';

// setAvatars()

class Avatars extends StatefulWidget {
  final String formId;
  Avatars(this.formId);

  @override
  _AvatarsState createState() => _AvatarsState(formId);
}

class _AvatarsState extends State<Avatars> {
  String formId;
  _AvatarsState(this.formId);

  int selected = 1;

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Upitnik',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: Container(
        color: Colors.teal[50],
        margin: EdgeInsets.all(10),
        child: ListView(
          children: [
            Text(
              'Odaberite avatar: ',
              style: TextStyle(
                fontSize: 20,
                //fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment
                    .center, //Center Row contents horizontally,

                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selected = 1;
                      }); // <-- replaced 'tapped' and 'other'
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: selected == 1
                            ? Border.all(
                                color: Colors.teal,
                                width: 1.0,
                              )
                            : Border.all(
                                color: Colors.transparent,
                              ),
                      ),
                      child: Image.asset('assets/images/female1_01.jpg',
                          width: (width - 30) / 2),
                    ),
                    /*
                    child: selected == 1
                        ? AddBorder('assets/images/female1_01.jpg')
                        : Image.asset('assets/images/female1_01.jpg',
                            width: (width-20)/2),
                    onTap: () {
                      print('female1_01 clicked');
                      selected = 1;
                      print('selected $selected');
                    },*/
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selected = 2;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: selected == 2
                            ? Border.all(
                                color: Colors.teal,
                                width: 1.0,
                              )
                            : Border.all(
                                color: Colors.transparent,
                              ),
                      ),
                      child: Image.asset('assets/images/male1_01.jpg',
                          width: (width - 30) / 2),
                    ),
                    /*
                    child: selected == 2
                        ? AddBorder('assets/images/male1_01.jpg')
                        : Image.asset('assets/images/male1_01.jpg',
                            width: (width - 20) / 2),*/
                    /*onTap: () {
                      print('male1_01 clicked');
                      selected = 2;
                      print('selected $selected');
                    },*/
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment
                    .center, //Center Row contents horizontally,

                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selected = 3;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: selected == 3
                            ? Border.all(
                                color: Colors.teal,
                                width: 1.0,
                              )
                            : Border.all(
                                color: Colors.transparent,
                              ),
                      ),
                      child: Image.asset('assets/images/female2_01.jpg',
                          width: (width - 30) / 2),
                    ), /*
                    child: selected == 3
                        ? AddBorder('assets/images/female2_01.jpg')
                        : Image.asset('assets/images/female2_01.jpg',
                            width: (width - 20) / 2),*/
                  )
                  /*onTap: () {
                      print('female2_01 clicked');
                      selected = 3;
                      print('selected $selected');
                    },*/
                  ,
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selected = 4;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: selected == 4
                            ? Border.all(
                                color: Colors.teal,
                                width: 1.0,
                              )
                            : Border.all(
                                color: Colors.transparent,
                              ),
                      ),
                      child: Image.asset('assets/images/male2_01.jpg',
                          width: (width - 30) / 2),
                    ),

                    /*selected == 4
                        ? AddBorder('assets/images/male02_01.jpg')
                        : Image.asset('assets/images/male2_01.jpg',
                            width: (width - 20) / 2),*/
                    /*onTap: () {
                      print('male2_01 clicked');
                      selected = 4;
                      print('selected $selected');
                    },*/
                  ),
                ],
              ),
            ),
          ],
        ),
        /*CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  HeaderWidget("Odaberi avatar:"),
                ],
              ),
            ),
            SliverGrid(
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
              delegate: SliverChildListDelegate(
                [
                  Avatars(),
                ],
              ),
            ),
          ],*/
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Potvrdi odabir avatara',
          child: Icon(Icons.check),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => (Name(formId, selected))),
            );
          }),
    );
  }
}
/*
class HeaderWidget extends StatelessWidget {
  final String text;

  HeaderWidget(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10, bottom: 2, top: 20),
      child: Text(
        text.toString(),
        style: TextStyle(
          fontSize: 20,
          //fontWeight: FontWeight.bold,
        ),
      ),
      color: Colors.teal[50],
    );
  }
}*/
/*
class Avatars extends StatelessWidget {
  Avatars();

  int selected;

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Container(
        color: Colors.teal[50],
        child: GridView.count(
          crossAxisCount: 2,
          childAspectRatio: 1.0,
          padding: const EdgeInsets.all(4.0),
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
          children: <Widget>[
            GestureDetector(
              child: selected == 1
                  ? AddBorder('assets/images/female1_01.jpg')
                  : Image.asset('assets/images/female1_01.jpg'),
              onTap: () {
                print('female1_01 clicked');
                selected = 1;
                print('selected $selected');
              },
            ),
            GestureDetector(
              child: selected == 2
                  ? AddBorder('assets/images/male1_01.jpg')
                  : Image.asset('assets/images/male1_01.jpg'),
              onTap: () {
                print('male1_01 clicked');
                selected = 2;
                print('selected $selected');
              },
            ),
            GestureDetector(
              child: selected == 3
                  ? AddBorder('assets/images/female2_01.jpg')
                  : Image.asset('assets/images/female2_01.jpg'),
              onTap: () {
                print('female2_01 clicked');
                selected = 3;
                print('selected $selected');
              },
            ),
            GestureDetector(
              child: selected == 4
                  ? AddBorder('assets/images/male02_01.jpg')
                  : Image.asset('assets/images/male2_01.jpg'),
              onTap: () {
                print('male2_01 clicked');
                selected = 4;
                print('selected $selected');
              },
            ),
          ],
        ),
      ),
    );
  }
}
*/

class AddBorder extends StatelessWidget {
  String image;

  AddBorder(this.image);

  @override
  Widget build(BuildContext context) {
    Container(
      decoration: BoxDecoration(
        color: const Color(0xff7c94b6),
        image: DecorationImage(
          image: ExactAssetImage(image),
          fit: BoxFit.cover,
        ),
        border: Border.all(
          color: Colors.red,
          width: 10.0,
        ),
      ),
    );
  }
}
