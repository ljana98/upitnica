import 'package:flutter/material.dart';
import 'package:gamiforms/questions/check_uncheck.dart';
import 'package:gamiforms/questions/checkboxes.dart';
import 'package:gamiforms/questions/like_dislike.dart';
import 'package:gamiforms/questions/linear_emoji.dart';
import 'package:gamiforms/questions/linear_grade.dart';
import 'package:gamiforms/questions/linear_scale.dart';
import 'package:gamiforms/questions/select_answer.dart';
import 'package:gamiforms/questions/text_box.dart';
import 'package:gamiforms/questions/yes_no.dart';
import 'package:gamiforms/user/avatar_progress.dart';
import 'package:gamiforms/user/scroll_physics.dart';

import 'package:gamiforms/home.dart';
import 'package:gamiforms/models/question_model.dart';
import 'package:gamiforms/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FillForm extends StatefulWidget {
  final String formId, name;
  final int selected;

  FillForm(this.formId, this.selected, this.name);

  @override
  _FillFormState createState() => _FillFormState();
}

int total = 0;
int _notAttempted = 0;

Stream infoStream;

class _FillFormState extends State<FillForm> {
  DatabaseService databaseService = DatabaseService();
  QuerySnapshot questionSnapshot;

  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    
    print("${widget.formId}");
    databaseService.getQuestionData(widget.formId).then((value) {
      questionSnapshot = value;
      total = questionSnapshot.documents.length;
      isLoading = false;

      print("$total this is total ${widget.formId}");

      setState(() {});
    });
  }

  QuestionModel getQuestionModelFromDatasnapshot(
      DocumentSnapshot questionSnapshot) {
    QuestionModel questionModel = QuestionModel();

    questionModel.question = questionSnapshot.data["question"];

    questionModel.option1 = "";
    questionModel.option2 = "";
    questionModel.option3 = "";
    questionModel.option4 = "";

    if (questionSnapshot.data["questionType"] == "CB" ||
        questionSnapshot.data["questionType"] == "SA") {
      questionModel.option1 = questionSnapshot.data["ans1"];
      questionModel.option2 = questionSnapshot.data["ans2"];
      questionModel.option3 = questionSnapshot.data["ans3"];
      questionModel.option4 = questionSnapshot.data["ans4"];
    }

    questionModel.questionType = questionSnapshot.data["questionType"];


    return questionModel;
  }

  @override
  void dispose() {
    infoStream = null;
    super.dispose();
  }

  var currentPageValue = 0;

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0);

    bool pic1 = false;
    bool pic2 = false;
    bool pic3 = false;
    bool pic4 = false;
    int qi = 0;
    int ind1 = ((questionSnapshot.documents.length + 4) / 4).round() - 1;
    int ind2 = ((questionSnapshot.documents.length + 4) / 2).round() - 1;
    int ind3 = (3 * (questionSnapshot.documents.length + 4) / 4).round() - 1;
    int ind4 = questionSnapshot.documents.length + 4 - 1;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            "Upitnik",
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: isLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()),
            )
          : Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: <Widget>[
                PageView.builder(
                  physics:
                      CustomScrollPhysics(), //new NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  controller: controller,
                  itemCount: questionSnapshot.documents.length + 4,
                  onPageChanged: (int page) {
                    getChangedPageAndMoveBar(page);
                  },
                  itemBuilder: (context, index) {
                    qi = index;
                    print("index $index");
                    if (index == ind1) {
                      print("ind1 $ind1");
                      pic1 = true;
                      return AvatarProgress(widget.selected, widget.name, true,
                          false, false, false);
                    }
                    if (index == ind2) {
                      //print("ind1 $ind2");
                      pic2 = true;
                      return AvatarProgress(widget.selected, widget.name, false,
                          true, false, false);
                    }
                    if (index == ind3) {
                      //print("ind1 $ind3");
                      pic3 = true;
                      return AvatarProgress(widget.selected, widget.name, false,
                          false, true, false);
                    }
                    if (index == ind4) {
                      //print("ind1 $ind4");
                      pic4 = true;
                      return AvatarProgress(widget.selected, widget.name, false,
                          false, false, true);
                    }

                    if ((index > ind1) && (index < ind2))
                      qi = index - 1;
                    else if ((index > ind2) && (index < ind3))
                      qi = index - 2;
                    else if ((index > ind3) && (index < ind4))
                      qi = index - 3;
                    else if (index > ind4) qi = index - 4;

                    print('FillFormTile QI $qi');
                    return FillFormTile(
                      formId: widget.formId,
                      questionModel: getQuestionModelFromDatasnapshot(
                          questionSnapshot.documents[qi]),
                      index: qi,
                      length: questionSnapshot.documents.length,
                      name: "name",
                      selected: 0,
                    );
                  },
                ),
                Stack(
                  alignment: AlignmentDirectional.topStart,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 100),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          for (int i = 0;
                              i < questionSnapshot.documents.length + 4;
                              i++)
                            if (i == currentPageValue) ...[circleBar(true)] else
                              circleBar(false),
                        ],
                      ),
                    ),
                  ],
                ),
                Stack(
                  children: <Widget>[
                    Positioned(
                      bottom: 20,
                      right: 20,
                      child: Visibility(
                        visible: currentPageValue ==
                                questionSnapshot.documents.length + 4 - 1
                            ? true
                            : false,
                        child: FloatingActionButton(
                          tooltip: 'Završi',
                          child: Icon(Icons.check),
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Home()));
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
    );
  }

  void getChangedPageAndMoveBar(int page) {
    currentPageValue = page;
    setState(() {});
  }

  Widget circleBar(bool isActive) {
    return Container(
      //duration: Duration (second: 150),
      margin: EdgeInsets.symmetric(horizontal: 8),
      height: isActive ? 12 : 8,
      width: isActive ? 12 : 8,
      decoration: BoxDecoration(
          color: isActive ? Colors.teal : Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(12))),
    );
  }
}

class FillFormTile extends StatefulWidget {
  final QuestionModel questionModel;
  final String name, formId;
  final int index, length, selected;

  FillFormTile(
      {@required this.formId,
      @required this.questionModel,
      @required this.index,
      @required this.length,
      @required this.name,
      @required this.selected});

  @override
  _FillFormTileState createState() => _FillFormTileState();
}

class _FillFormTileState extends State<FillFormTile> {
  String optionSelected = "";

  @override
  Widget build(BuildContext context) {
    print('index ${widget.index}');

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Container(
            child: widget.questionModel.questionType == 'LG'
                ? LinearGrade(widget.questionModel.question, widget.formId)
                : widget.questionModel.questionType == 'LS'
                    ? LinearScale(widget.questionModel.question, widget.formId)
                    : widget.questionModel.questionType == 'LE'
                        ? LinearEmoji(
                            widget.questionModel.question, widget.formId)
                        : widget.questionModel.questionType == 'CU'
                            ? CheckUncheck(
                                widget.questionModel.question, widget.formId)
                            : widget.questionModel.questionType == 'LD'
                                ? LikeDislike(widget.questionModel.question,
                                    widget.formId)
                                : widget.questionModel.questionType == 'TB'
                                    ? TextBox(widget.questionModel.question,
                                        widget.formId)
                                    : widget.questionModel.questionType == 'YN'
                                        ? YesNo(widget.questionModel.question,
                                            widget.formId)
                                        : widget.questionModel.questionType ==
                                                'SA'
                                            ? SelectAnswer(widget.questionModel,
                                                widget.formId)
                                            /*: widget.questionModel.questionType ==
                                                'CB'
                                            ? Checkboxes(widget.questionModel)*/
                                            : Text(
                                                widget.questionModel.question),
          ),
          SizedBox(height: 4),
        ],
      ),
    );
  }
}

class GetQuestion extends StatelessWidget {
  final String documentId;

  GetQuestion(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');

    return FutureBuilder<QuerySnapshot>(
      future: questionairres
          .document(documentId)
          .collection('questions')
          .getDocuments(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.documents.first.data;
          return data['question'];
        }
      },
    );
  }
}

class GetQuestionType extends StatelessWidget {
  final String documentId;

  GetQuestionType(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');

    return FutureBuilder<QuerySnapshot>(
      future: questionairres
          .document(documentId)
          .collection('questions')
          .getDocuments(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.documents.first.data;
          return data['questionType'];
        }
      },
    );
  }
}

class Duration extends StatelessWidget {
  final String documentId;

  Duration(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');

    return FutureBuilder<DocumentSnapshot>(
      future: questionairres.document(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data;
          return Container(
            width: 300,
            child: Align(
              alignment: Alignment.center,
              child: Text(
                'Trajanje upitnika: ${data['formDuration']} min.',
                style: TextStyle(fontSize: 18),
              ),
            ),
          );
        }
      },
    );
  }
}
