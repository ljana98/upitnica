import 'package:flutter/material.dart';
import 'package:gamiforms/user/fill_form.dart';

class Name extends StatefulWidget {
  String formId;
  int selected;

  Name(this.formId, this.selected);

  @override
  State<StatefulWidget> createState() => _NameState(formId, selected);
}

class _NameState extends State<Name> {
  String formId;
  int selected;

  _NameState(this.formId, this.selected);

   GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  final myController = TextEditingController();

  /*@override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }  */

  String _name = '';

  void _updateName(String name) {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Upitnik',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20),
                child: _buildInputForm(),
              ),
              Padding(
                padding: EdgeInsets.only(left: 40, right: 40),
                child: selected == 2
                    ? Image.asset('assets/images/male1_01.jpg')
                    : selected == 3
                        ? Image.asset('assets/images/female2_01.jpg')
                        : selected == 4
                            ? Image.asset('assets/images/male2_01.jpg')
                            : Image.asset('assets/images/female1_01.jpg'),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          tooltip: 'Potvrdi naziv avatara',
          child: Icon(Icons.check),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => (FillForm(formId, selected, myController.text)),),
            );
          }),
    );
  }

  Widget _buildInputForm() {
    return Form(
      key: formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: myController,
            decoration: InputDecoration(
              labelText: 'Unesi naziv avatara',
              labelStyle: TextStyle(fontSize: 18),
            ),
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) =>
                val.isEmpty ? 'Naziv avatara mora biti unesen' : null,
            //onSaved: (name) => _name = name,
            //onFieldSubmitted: (name) => _updateName(name),
          ),
        ],
      ),
    );
  }
}

/*
class _NameState extends State<AvatarPage> {
  static final formKey = new GlobalKey<FormState>();

  String _name = '';

  FocusNode _focusNode = new FocusNode();

  void _updateName(String name) {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      setState(() {});
      print('Saved: $_name');
    }
  }

  void _clear() {
    final form = formKey.currentState;
    form.reset();
    FocusScope.of(context).requestFocus(_focusNode);
  }

  @override
  Widget build(BuildContext context) {
    var children = [
      _buildInputForm(),
    ];
    if (_name.length > 0) {
      var url = 'https://robohash.org/$_name';
      var avatar = new Avatar(url: url, size: 150.0);
      children.addAll([
        new VerticalPadding(child: avatar),
        new VerticalPadding(child: new Text('Courtesy of robohash.org')),
      ]);
    }

    children.addAll([
      new Expanded(child: Container()),
      new Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
        new VerticalPadding(
            child: new FlatButton(
          child: new Text('Clear', style: new TextStyle(fontSize: 24.0)),
          onPressed: _clear,
        ))
      ])
    ]);

    return new Scaffold(
      appBar: new AppBar(title: new Text('Greeting, Robot')),
      body: new Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: new Center(
          child: new Column(
            children: children,
          ),
        ),
      ),
    );
  }

  Widget _buildInputForm() {
    var children = [
      new VerticalPadding(
          child: new TextFormField(
        focusNode: _focusNode,
        decoration: new InputDecoration(
          labelText: 'Enter your unique identifier',
          labelStyle: new TextStyle(fontSize: 20.0),
        ),
        style: new TextStyle(fontSize: 24.0, color: Colors.black),
        validator: (val) => val.isEmpty ? 'Name can\'t be empty' : null,
        onSaved: (name) => _name = name,
        onFieldSubmitted: (name) => _updateName(name),
      ))
    ];

    return new Form(
      key: formKey,
      child: new Column(
        children: children,
      ),
    );
  }
}

class VerticalPadding extends StatelessWidget {
  VerticalPadding({this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: child,
    );
  }
}*/
