import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gamiforms/home.dart';
import 'package:gamiforms/services/database.dart';
import 'package:gamiforms/user/agreement.dart';
import 'package:gamiforms/user/avatars.dart';

class UserFormHome extends StatefulWidget {
  final String formId;
  UserFormHome(this.formId);

  @override
  _UserFormHomeState createState() => _UserFormHomeState(formId);
}

int total = 0;
int _notAttempted = 0;

Stream infoStream;

class _UserFormHomeState extends State<UserFormHome> {
  String formId;

  _UserFormHomeState(this.formId);

  DatabaseService databaseService = DatabaseService();
  Stream formStream;
  QuerySnapshot questionSnapshot;

  DocumentSnapshot snapshot;

  bool isLoading = true;

  @override
  void initState() {
    print("${widget.formId}");

    databaseService.getFormData().then((value) {
      formStream = value;
      setState(() {});
    });

    databaseService.getQuestionData(widget.formId).then((value) {
      questionSnapshot = value;
      total = questionSnapshot.documents.length;
      isLoading = false;

      print("$total this is total ${widget.formId}");

      setState(() {});
    });

    super.initState();
  }

  bool printed = false;
  @override
  Widget build(BuildContext context) {
    CollectionReference questionairres = Firestore.instance.collection('forms');
    return FutureBuilder<DocumentSnapshot>(
        future: questionairres.document(formId).get(),
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> data = snapshot.data.data;
            var screenSize = MediaQuery.of(context).size;
            var width = screenSize.width;
            return data == null
                ? Scaffold(
                    resizeToAvoidBottomInset: false,
                    appBar: AppBar(
                      title: Center(
                        child: Text(
                          'Nepostojeći upitnik',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      backgroundColor: Colors.teal,
                    ),
                    //body: Text('This is my default text!'),
                    //stavljamo djecu za vise widgeta unutar jednog -> stablo
                    body: Container(
                      margin: EdgeInsets.only(left: 10, right: 10, top: 50),
                      width: width,
                      child: Align(
                        //alignment: Alignment.topCenter,
                        child: Column(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          //crossAxisAlignment: CrossAxisAlignment.center,

                          children: <Widget>[
                            Text(
                              'Upitnik nije pronađen\n',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Upitnik ne postoji ili ste unijeli pogrešan ID upitnika. Provjerite je li ID upitnika točan i unesite ga ponovno.\n',
                              style: TextStyle(fontSize: 20),
                            ),
                          ],
                        ),
                      ),
                    ),
                    floatingActionButton: FloatingActionButton(
                        tooltip: 'U redu',
                        child: Icon(Icons.check),
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => (Home())),
                          );
                        }),
                  )
                : Scaffold(
                    appBar: AppBar(
                      title: Center(
                        child: Text(
                          'Upitnik',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      backgroundColor: Colors.teal,
                    ),
                    body: Container(
                      child: ListView(
                        children: [
                          StreamBuilder(
                            stream: formStream,
                            builder: (context, snapshot) {
                              return snapshot.data == null
                                  ? Container()
                                  : ListView.builder(
                                      shrinkWrap: true,
                                      physics: ClampingScrollPhysics(),
                                      itemCount: snapshot.data.documents.length,
                                      itemBuilder: (context, index) {
                                        while (widget.formId !=
                                                snapshot.data.documents[index]
                                                    .data['formId'] &&
                                            index <
                                                snapshot.data.documents.length -
                                                    1) {
                                          index++;
                                        }
                                        if (widget.formId ==
                                                snapshot.data.documents[index]
                                                    .data['formId'] &&
                                            printed == false) {
                                          printed = true;
                                          return FillFormTile(
                                            title: snapshot
                                                .data
                                                .documents[index]
                                                .data['formTitle'],
                                            imageUrl: snapshot
                                                .data
                                                .documents[index]
                                                .data['formImageUrl'],
                                            description: snapshot
                                                .data
                                                .documents[index]
                                                .data['formDesc'],
                                            id: snapshot.data.documents[index]
                                                .data['formId'],
                                            agree: snapshot.data
                                                .documents[index].data['agree'],
                                          );
                                        }
                                      });
                            },
                          )
                        ],
                      ),
                    ),
                  );
          }
        });
  }
}

class FillFormTile extends StatelessWidget {
  final String title, imageUrl, id, description;
  final bool agree;

  FillFormTile(
      {this.title, this.imageUrl, this.description, this.id, this.agree});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Stack(
                children: [Image.network(imageUrl, fit: BoxFit.cover)],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              description,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
          ),
          RaisedButton(
              color: Colors.teal,
              textColor: Colors.white,
              child: Text('Započni rješavanje upitnika'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: agree
                          ? (context) => (Agreement(id))
                          : (context) => Avatars(id)),
                );
              }),
          Text(
            "",
            style: TextStyle(fontSize: 30),
          ),
        ],
      ),
    );
  }
}
