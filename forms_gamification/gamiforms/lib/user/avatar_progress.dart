import 'package:flutter/material.dart';

class AvatarProgress extends StatelessWidget {
  final String name;
  final int selected;
  final bool pic1, pic2, pic3, pic4;

  AvatarProgress(
      this.selected, this.name, this.pic1, this.pic2, this.pic3, this.pic4);

  String picture, message;

  @override
  Widget build(BuildContext context) {
    if (selected == 1) {
      if (pic1 == true) {
        picture = 'assets/images/female1_02.jpg';
        message = 'Odličan početak, $name!';
      }
      if (pic2 == true) {
        picture = 'assets/images/female1_03.jpg';
        message = '$name, pola puta je prijeđeno.\nSamo naprijed!';
      }
      if (pic3 == true) {
        picture = 'assets/images/female1_04.jpg';
        message = 'Još samo malo!';
      }
      if (pic4 == true) {
        picture = 'assets/images/female1_05.jpg';
        message = 'Bravo, $name!\nHvala na sudjelovanju u upitniku.';
      }
    }
    if (selected == 2) {
      if (pic1 == true) {
        picture = 'assets/images/male1_02.jpg';
        message = 'Odličan početak, $name!';
      }
      if (pic2 == true) {
        picture = 'assets/images/male1_03.jpg';
       message = '$name, pola puta je prijeđeno.\nSamo naprijed!';
      }
      if (pic3 == true) {
        picture = 'assets/images/male1_04.jpg';
        message = 'Još samo malo!';
      }
      if (pic4 == true) {
        picture = 'assets/images/male1_05.jpg';
        message = 'Bravo, $name!\nHvala na sudjelovanju u upitniku.';
      }
    }
    if (selected == 3) {
      if (pic1 == true) {
        picture = 'assets/images/female2_02.jpg';
        message = 'Odličan početak, $name!';
      }
      if (pic2 == true) {
        picture = 'assets/images/female2_03.jpg';
        message = '$name, pola puta je prijeđeno.\nSamo naprijed!';
      }
      if (pic3 == true) {
        picture = 'assets/images/female2_04.jpg';
        message = 'Još samo malo!';
      }
      if (pic4 == true) {
        picture = 'assets/images/female2_05.jpg';
        message = 'Bravo, $name!\nHvala na sudjelovanju u upitniku.';
      }
    }
    if (selected == 4) {
      if (pic1 == true) {
        picture = 'assets/images/male2_02.jpg';
        message = 'Odličan početak, $name!';
      }
      if (pic2 == true) {
        picture = 'assets/images/male2_03.jpg';
        message = '$name, pola puta je prijeđeno.\nSamo naprijed!';
      }
      if (pic3 == true) {
        picture = 'assets/images/male2_04.jpg';
        message = 'Još samo malo!';
      }
      if (pic4 == true) {
        picture = 'assets/images/male2_05.jpg';
        message = 'Bravo, $name!\nHvala na sudjelovanju u upitniku.';
      }
    }

    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    return SizedBox(
      width: width,
      height: height - 100,
      child: Container(
        color: Colors.teal[50],
        margin: EdgeInsets.all(10),
        child: Align(
          alignment: Alignment.center,
          child: Column(
            children: [
              SizedBox(height: 30),
              Text(
                message,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.center,
                child: ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: 5.0,
                    minWidth: 5.0,
                    maxHeight: height-200,
                    maxWidth: width,
                  ),
                  child: Image.asset(picture),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
