import 'package:flutter/material.dart';
import 'package:gamiforms/helper/constants.dart';
import 'package:gamiforms/home.dart';
import 'admin/log_home.dart';


void main() {
  runApp(MyApp());
} 

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool isUserLoggedIn = false;
/*
  checkUserLoggedInStatus() async {
    isUserLoggedIn = await Constants.getUserLoggedInSharedPreference();
  }
*/
  getLoggedInState() async {
    await Constants.getUserLoggedInSharedPreference().then((value) {
      setState(() {
        isUserLoggedIn = value;
      });
    });
  }

  @override
  void initState() {
    getLoggedInState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.teal[50],
        primarySwatch: Colors.teal,
      ),
      home: (isUserLoggedIn ?? false) ? LogHome() : Home(),
    );
  }
}
