import 'package:flutter/material.dart';
import 'package:gamiforms/services/database.dart';

class LinearGrade extends StatefulWidget {
  String question, formId;

  LinearGrade(this.question, this.formId);

  @override
  LinearGradeState createState() => LinearGradeState(question);
}

class LinearGradeState extends State<LinearGrade> {
  String question;

  LinearGradeState(this.question);

  double overall = 3.0;
  String overallStatus = "3";

  static final formKey = new GlobalKey<FormState>();

  DatabaseService databaseService = DatabaseService();

  String answer = "";

  bool isLoading = false;

  bool enabled = true;

  uploadFormData(overall) {
    setState(() {
      isLoading = true;
    });

    print('overall $overall');

    if (overall == 1.0) answer = "1";
    if (overall == 2.0) answer = "2";
    if (overall == 3.0) answer = "3";
    if (overall == 4.0) answer = "4";
    if (overall == 5.0) answer = "5";

    print('answer $answer');

    Map<String, String> answerMap = {
      "question": this.question,
      "answer": answer,
    };

    print("${widget.formId}");
    databaseService.addAnswerData(answerMap, widget.formId).then((value) {
      answer = "";
      question = this.question;
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    print('TEST $question');
    return SizedBox(
      width: width,
      height: height - 100,
      child: Container(
        margin: EdgeInsets.only(top: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
                margin: EdgeInsets.only(left: 16.0, bottom: 70),
                child: Text(
                  question,
                  style: TextStyle(fontSize: 20),
                )),
            Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              child: Text(
                overallStatus,
                style: TextStyle(
                    color: Colors.teal[800],
                    fontWeight: FontWeight.bold,
                    fontSize: 100.0),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Center(
                child: Slider(
                  value: overall,
                  onChanged: enabled
                      ? (value) {
                          setState(() {
                            overall = value.round().toDouble();
                            _getOverallStatus(overall);
                          });
                        }
                      : null,
                  onChangeEnd: enabled
                      ? (value) {
                          enabled = false;
                          setState(() {
                            overall = value.round().toDouble();
                            uploadFormData(overall);
                          });
                        }
                      : null,
                  label: '${overall.toInt()}',
                  divisions: 4,
                  min: 1.0,
                  max: 5.0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _getOverallStatus(double overall) {
    switch (overall.toInt()) {
      case 1:
        overallStatus = '1';
        break;
      case 2:
        overallStatus = '2';
        break;
      case 3:
        overallStatus = '3';
        break;
      case 4:
        overallStatus = '4';
        break;
      default:
        overallStatus = '5';
        break;
    }
  }
}
