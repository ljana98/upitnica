import 'package:flutter/material.dart';
import 'package:gamiforms/services/database.dart';

class YesNo extends StatefulWidget {
  String question, formId;

  YesNo(this.question, this.formId);

  @override
  State<StatefulWidget> createState() => YesNoState(this.question);
}

class YesNoState extends State<YesNo> {
  String question;

  YesNoState(this.question);

  String selected = 'DA';

  static final formKey = new GlobalKey<FormState>();

  DatabaseService databaseService = DatabaseService();

  String questionId = "", answer = "";

  bool isLoading = false;

  bool enabled = true;

  uploadFormData(answer) {
    //if (formKey.currentState.validate()) {
    setState(() {
      isLoading = true;
    });

    Map<String, String> answerMap = {
      "question": question,
      "answer": answer,
    };

    print("${widget.formId}");
    databaseService.addAnswerData(answerMap, widget.formId).then((value) {
      answer = "";
      question = widget.question;
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    return SizedBox(
      width: width,
      height: height - 100,
      child: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            SizedBox(height: 20),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                question.toString(),
                style: TextStyle(fontSize: 20),
              ),
            ),
            SizedBox(height: (height) / 2 - 200),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: enabled
                          ? () {
                              enabled = false;
                              print('yes clicked');
                              setState(() {
                                selected = 'DA';
                                uploadFormData(selected);
                              });
                            }
                          : null,
                      child: Container(
                        decoration: BoxDecoration(
                          border: selected == 'DA'
                              ? Border.all(
                                  color: Colors.teal,
                                  width: 1.0,
                                )
                              : Border.all(
                                  color: Colors.transparent,
                                ),
                        ),
                        child: Text(
                          'DA',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 70),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: enabled
                          ? () {
                              enabled = false;
                              print('no clicked');
                              setState(() {
                                selected = 'NE';
                                uploadFormData(selected);
                              });
                            }
                          : null,
                      child: Container(
                        decoration: BoxDecoration(
                          border: selected == 'NE'
                              ? Border.all(
                                  color: Colors.teal,
                                  width: 1.0,
                                )
                              : Border.all(
                                  color: Colors.transparent,
                                ),
                        ),
                        child: Text(
                          'NE',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class HeaderWidget extends StatelessWidget {
  final String text;

  HeaderWidget(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10, bottom: 2, top: 20),
      child: Text(
        text.toString(),
        style: TextStyle(fontSize: 20),
      ),
      color: Colors.teal[50],
    );
  }
}
