import 'package:flutter/material.dart';
import 'package:gamiforms/services/database.dart';

class LinearScale extends StatefulWidget {
  String question, formId;

  LinearScale(this.question, this.formId);

  @override
  LinearScaleState createState() => LinearScaleState(question);
}

class LinearScaleState extends State<LinearScale> {
  String question;

  LinearScaleState(this.question);

  double overall = 3.0;
  String overallStatus = "Dobro";

  static final formKey = new GlobalKey<FormState>();

  DatabaseService databaseService = DatabaseService();

  String answer = "";

  bool isLoading = false;

  bool enabled = true;

  uploadFormData(overall) {
    setState(() {
      isLoading = true;
    });

    print('overall $overall');

    if (overall == 1.0) answer = "Užasno";
    if (overall == 2.0) answer = "Loše";
    if (overall == 3.0) answer = "Dobro";
    if (overall == 4.0) answer = "Vrlo dobro";
    if (overall == 5.0) answer = "Odlično";

    print('answer $answer');

    Map<String, String> answerMap = {
      "question": this.question,
      "answer": answer,
    };

    print("${widget.formId}");
    databaseService.addAnswerData(answerMap, widget.formId).then((value) {
      answer = "";
      question = this.question;
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    print('TEST $question');
    return SizedBox(
      width: width,
      height: height - 100,
      child: Container(
        margin: EdgeInsets.only(top: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
                margin: EdgeInsets.only(left: 16.0, bottom: 100),
                child: Text(
                  question,
                  style: TextStyle(fontSize: 20),
                )),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                overallStatus,
                style: TextStyle(
                    color: Colors.teal[800],
                    fontWeight: FontWeight.bold,
                    fontSize: 45.0),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Center(
                child: Slider(
                  value: overall,
                  onChanged: enabled
                      ? (value) {
                          setState(() {
                            overall = value.round().toDouble();
                            _getOverallStatus(overall);
                          });
                        }
                      : null,
                  onChangeEnd: enabled
                      ? (value) {
                          enabled = false;
                          setState(() {
                            overall = value.round().toDouble();
                            uploadFormData(overall);
                          });
                        }
                      : null,
                  label: '${overall.toInt()}',
                  divisions: 4,
                  min: 1.0,
                  max: 5.0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _getOverallStatus(double overall) {
    switch (overall.toInt()) {
      case 1:
        overallStatus = 'Loše';
        break;
      case 2:
        overallStatus = 'Prihvatljivo';
        break;
      case 3:
        overallStatus = 'Dobro';
        break;
      case 4:
        overallStatus = 'Vrlo dobro';
        break;
      default:
        overallStatus = 'Odlično';
        break;
    }
  }
}
