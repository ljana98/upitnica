/*import 'package:flutter/material.dart';
import 'package:gamiforms/models/question_model.dart';

import 'text_box.dart';


class Checkboxes extends StatefulWidget {
  QuestionModel model;

  Checkboxes(this.model);

  @override
  CheckboxesState createState() => new CheckboxesState(model);
}

class CheckboxesState extends State {
  QuestionModel model;
  //String question, option1, option2, option3, option4;

  CheckboxesState(this.model);

  Map<String, bool> values;

  var tmpArray = [];

  getMapValues() {
    //print('option1 $option1');
    values = {
      this.model.option1: false,
      this.model.option2: false,
      this.model.option3: false,
      this.model.option4: false,
    };
    //print('question $question');
  }

  getCheckboxItems() {
    getMapValues();
    values.forEach((key, value) {
      if (value == true) {
        tmpArray.add(key);
      }
    });

    // Printing all selected items on Terminal screen.
    print(tmpArray);
    // Here you will get all your selected Checkbox items.

    // Clear array after use.
    tmpArray.clear();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    print('values1 $values');
    getMapValues();
    print('values2 $values');
    return /*Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Center(
          child: Text(
            'Susretnica - upitnik',
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.teal,
      ),
      body: */
        SizedBox(
      width: width,
      height: height-400,
      child: Column(
        mainAxisAlignment:
            MainAxisAlignment.center, //Center Column contents vertically,
        crossAxisAlignment:
            CrossAxisAlignment.center, //Center Column contents horizontally,
        children: <Widget>[
          SizedBox(height: 15),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                this.model.question,
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          SizedBox(height: 20),
          Expanded(
            //child: Card(
              child: Column(
                children: values.keys.map((String key) {
                  return CheckboxListTile(
                    title: Text(key),
                    value: values[key],
                    activeColor: Colors.tealAccent[700],
                    checkColor: Colors.white,
                    selectedTileColor: Colors.teal,
                    onChanged: (bool value) {
                      setState(() {
                        values[key] = value;
                      });
                    },
                  );
                }).toList(),
              ),
              //),
            ),
          
        ],
      ), /*
      floatingActionButton: FloatingActionButton(
          tooltip: 'Increment',
          child: Icon(Icons.arrow_forward),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => (TextBox())),
            );
          }),*/
    );
  }
}
*/