import 'package:flutter/material.dart';
import 'package:gamiforms/models/question_model.dart';
import 'package:gamiforms/services/database.dart';

class SelectAnswer extends StatefulWidget {
  QuestionModel model;
  String formId;

  SelectAnswer(this.model, this.formId);

  @override
  SelectAnswerState createState() {
    return SelectAnswerState(model);
  }
}

class SelectAnswerState extends State<SelectAnswer> {
  QuestionModel model;

  SelectAnswerState(this.model);

  int answer;

  Map<String, bool> values;

  var tmpArray = [];

  getMapValues() {
    //print('option1 $option1');
    values = {
      this.model.option1: false,
      this.model.option2: false,
      this.model.option3: false,
      this.model.option4: false,
    };
    //print('question $question');
  }

  static final formKey = new GlobalKey<FormState>();

  DatabaseService databaseService = DatabaseService();

  String questionId = "";

  bool isLoading = false;

  String question;

  String finalAnswer;

  uploadFormData(answer) {
    //if (formKey.currentState.validate()) {

    question = this.model.question;
    setState(() {
      isLoading = true;
    });

    if (answer == 1) {
      finalAnswer = this.model.option1.toString();
    }
    if (answer == 2) {
      finalAnswer = this.model.option2.toString();
    }
    if (answer == 3) {
      finalAnswer = this.model.option3.toString();
    }
    if (answer == 4) {
      finalAnswer = this.model.option4.toString();
    }

    Map<String, String> answerMap = {
      "question": question,
      "answer": finalAnswer,
    };

    print("${widget.formId}");
    databaseService.addAnswerData(answerMap, widget.formId).then((value) {
      answer = "";
      question = model.question;
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  getSelectAnswerItems() {
    getMapValues();
    values.forEach((key, value) {
      if (value == true) {
        tmpArray.add(key);
      }
    });
    print(tmpArray);
    tmpArray.clear();
  }

  bool enabled = true;

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    print('SA values1 $values');
    getMapValues();
    print('SA values2 $values');

    int value;

    return SizedBox(
        width: width,
        height: height - 100,
        child: Column(
          children: [
            SizedBox(height: 15),
            Padding(
              padding: EdgeInsets.all(15),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  this.model.question,
                  style: TextStyle(fontSize: 18),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            SizedBox(height: 20),
            Row(
              children: [
                Radio(
                    activeColor: Colors.tealAccent[700],
                    value: 1,
                    groupValue: answer,
                    onChanged: (val) {
                      enabled == true
                          ? setState(() {
                              enabled = false;
                              answer = val;
                              uploadFormData(answer);
                              print("value $value");
                            })
                          : null;
                    }),
                Text(this.model.option1)
              ],
            ),
            Row(
              children: [
                Radio(
                    activeColor: Colors.tealAccent[700],
                    value: 2,
                    groupValue: answer,
                    onChanged: (val) {
                      enabled == true
                          ? setState(() {
                              enabled = false;
                              uploadFormData(answer);
                              answer = val;
                            })
                          : null;
                    }),
                Text(this.model.option2)
              ],
            ),
            Row(
              children: [
                Radio(
                    activeColor: Colors.tealAccent[700],
                    value: 3,
                    groupValue: answer,
                    onChanged: (val) {
                      enabled == true
                          ? setState(() {
                              enabled = false;
                              answer = val;
                              uploadFormData(answer);
                            })
                          : null;
                    }),
                Text(this.model.option3)
              ],
            ),
            Row(
              children: [
                Radio(
                    activeColor: Colors.tealAccent[700],
                    value: 4,
                    groupValue: answer,
                    onChanged: (val) {
                      enabled == true
                          ? setState(() {
                              enabled = false;
                              answer = val;
                              uploadFormData(answer);
                            })
                          : null;
                    }),
                Text(this.model.option4)
              ],
            ),
          ],
        ));
  }
}