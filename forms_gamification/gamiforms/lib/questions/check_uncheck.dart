import 'package:flutter/material.dart';
import 'package:gamiforms/services/database.dart';

import 'like_dislike.dart';

class CheckUncheck extends StatefulWidget {
  String question, formId;

  CheckUncheck(this.question, this.formId);

  @override
  State<StatefulWidget> createState() => CheckUncheckState(this.question);
}

class CheckUncheckState extends State<CheckUncheck> {
  String question;

  CheckUncheckState(this.question);

  String selected = 'Kvačica';

  static final formKey = new GlobalKey<FormState>();

  DatabaseService databaseService = DatabaseService();

  String questionId = "", answer = "";

  bool isLoading = false;

  bool enabled = true;

  uploadFormData(answer) {
    setState(() {
      isLoading = true;
    });

    Map<String, String> answerMap = {
      "question": question,
      "answer": answer,
    };

    print("${widget.formId}");
    databaseService.addAnswerData(answerMap, widget.formId).then((value) {
      answer = "";
      question = widget.question;
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;

    return 
        SizedBox(
      width: width,
      height: height - 100,
      child: Container(
        color: Colors.teal[50],
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            SizedBox(height: 20),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                question.toString(),
                style: TextStyle(fontSize: 20),
              ),
            ),
            SizedBox(height: (height) / 2 - 200),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment
                    .center, 
                children: [
                  GestureDetector(
                    onTap: enabled
                        ? () {
                            enabled = false;
                            print('check clicked');
                            setState(() {
                              selected = 'Kvačica';
                              uploadFormData(selected);
                            });
                          }
                        : null,
                    child: Container(
                      decoration: BoxDecoration(
                        border: selected == 'Kvačica'
                            ? Border.all(
                                color: Colors.teal,
                                width: 1.0,
                              )
                            : Border.all(
                                color: Colors.transparent,
                              ),
                      ),
                      child: Image.asset(
                        'assets/images/check.png',
                        width: 130,
                        height: 130,
                      ),
                    ),
                  ),
                  SizedBox(width: 30),
                  GestureDetector(
                    onTap: enabled
                        ? () {
                            enabled = false;
                            print('uncheck clicked');
                            setState(() {
                              selected = 'Iksić';
                              uploadFormData(selected);
                            });
                          }
                        : null,
                    child: Container(
                      decoration: BoxDecoration(
                        border: selected == 'Iksić'
                            ? Border.all(
                                color: Colors.teal,
                                width: 1.0,
                              )
                            : Border.all(
                                color: Colors.transparent,
                              ),
                      ),
                      child: Image.asset(
                        'assets/images/uncheck.png',
                        width: 130,
                        height: 130,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
