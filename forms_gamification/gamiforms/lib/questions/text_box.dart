import 'package:flutter/material.dart';
import 'package:gamiforms/services/database.dart';

class TextBox extends StatefulWidget {
  String question, formId;

  TextBox(this.question, this.formId);

  @override
  State<StatefulWidget> createState() => _TextBoxState(question);
}

class _TextBoxState extends State<TextBox> {
  String question;

  _TextBoxState(this.question);

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  DatabaseService databaseService = DatabaseService();

  String answer = "";

  bool isLoading = false;

  bool enabled = true;

  uploadFormData(answer) {
    if (formKey.currentState.validate()) {
      setState(() {
        isLoading = true;
      });

      Map<String, String> answerMap = {
        "question": this.question,
        "answer": answer,
      };

      print("${widget.formId}");
      databaseService.addAnswerData(answerMap, widget.formId).then((value) {
        answer = "";
        question = this.question;
        setState(() {
          isLoading = false;
        });
      }).catchError((e) {
        print(e);
      });
    } else {
      print("error is happening ");
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;

    print('TEST $question');
    return SizedBox(
      width: width,
      height: height - 100,
      child: Container(
        margin: EdgeInsets.all(10),
        child: Center(
          child: Stack(
            children: <Widget>[
              _buildInputForm(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildInputForm() {
    return Form(
      key: formKey,
      child: ListView(
        children: <Widget>[
          SizedBox(height: 20),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              this.question,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 20),
            ),
          ),
          SizedBox(height: 30),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Vaš odgovor',
              labelStyle: TextStyle(fontSize: 18),
            ),
            style: TextStyle(fontSize: 20, color: Colors.black),
            validator: (val) => val.isEmpty ? 'Odgovor mora biti unesen' : null,
            onSaved: enabled ? (answer) => answer = answer : null,
            onFieldSubmitted: enabled
                ? (answer) {
                    enabled = false;
                    uploadFormData(answer);
                  }
                : null,
          )
        ],
      ),
    );
  }
}
