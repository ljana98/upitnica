import 'package:flutter/material.dart';
import 'package:gamiforms/services/database.dart';

class LinearEmoji extends StatefulWidget {
  String question, formId;

  LinearEmoji(this.question, this.formId);

  @override
  LinearEmojiState createState() => LinearEmojiState(question);
}

class LinearEmojiState extends State<LinearEmoji> {
  String question;

  LinearEmojiState(this.question);

  double overall = 3.0;
  String overallStatus = 'assets/images/emoji_3.png';

  static final formKey = new GlobalKey<FormState>();

  DatabaseService databaseService = DatabaseService();

  String answer = "";

  bool isLoading = false;

  bool enabled = true;

  uploadFormData(overall) {
    //if (formKey.currentState.validate()) {
    setState(() {
      isLoading = true;
    });

    print('overall $overall');

    if (overall == 1.0) answer = "Emotikon za 1 :(";
    if (overall == 2.0) answer = "Emotikon za 2 :/";
    if (overall == 3.0) answer = "Emotikon za 3 :|";
    if (overall == 4.0) answer = "Emotikon za 4 :)";
    if (overall == 5.0) answer = "Emotikon za 5 :D";

    print('answer $answer');

    Map<String, String> answerMap = {
      "question": this.question,
      "answer": answer,
    };

    print("${widget.formId}");
    databaseService.addAnswerData(answerMap, widget.formId).then((value) {
      answer = "";
      question = this.question;
      setState(() {
        isLoading = false;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var width = screenSize.width;
    var height = screenSize.height;
    print('TEST $question');
    return SizedBox(
      width: width,
      height: height - 100,
      child: Container(
        margin: EdgeInsets.only(top: 30, left: 10, right: 10, bottom: 10),
        child: Column(
          children: [
            Container(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  question,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            SizedBox(height: 20),
            Image.asset(overallStatus, width: width / 1.5),
            Expanded(
              child: Center(
                child: Slider(
                  value: overall,
                  onChanged: enabled
                      ? (value) {
                          enabled = false;
                          setState(() {
                            overall = value.round().toDouble();
                            _getOverallStatus(overall);
                          });
                        }
                      : null,
                  onChangeEnd: enabled
                      ? (value) {
                          enabled = false;
                          setState(() {
                            overall = value.round().toDouble();
                            uploadFormData(overall);
                          });
                        }
                      : null,
                  label: '${overall.toInt()}',
                  divisions: 4,
                  min: 1.0,
                  max: 5.0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _getOverallStatus(double overall) {
    switch (overall.toInt()) {
      case 1:
        overallStatus = 'assets/images/emoji_1.png';
        break;
      case 2:
        overallStatus = 'assets/images/emoji_2.png';
        break;
      case 3:
        overallStatus = 'assets/images/emoji_3.png';
        break;
      case 4:
        overallStatus = 'assets/images/emoji_4.png';
        break;
      default:
        overallStatus = 'assets/images/emoji_5.png';
        break;
    }
  }
}
